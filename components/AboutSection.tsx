import React from "react"
import Image from "next/image"

const skills = [
    {skill: "HTML"},
    {skill: "CSS"},
    {skill: "JavaScript"},
    {skill: "React.js"},
    {skill: "FastAPI"},
    {skill: "Git"},
    {skill: "Tailwind CSS"},
    {skill: "PostgreSQL"},
    {skill: "Redux.js"},
    {skill: "Django"},
    {skill: "Docker"},
]

const AboutSection = () => {
    return (
        <section id="about">
            <div className="my-12 pb-12 md:pt-16 md:pb-48">
                <h1 className="text-center font-bold text-4xl">About me
                <hr className="w-6 h-1 mx-auto my-4 bg-blue-500 border-0 rounded"></hr>
                </h1>
                <div className="flex flex-col space-y-10 items-stretch justify-center align-top md:space-x-10 md:space-y-0 md:p-4 md:flex-row md:text-left">
                    <div className="md:w-1/2">
                        <h1 className="text-center text-2xl font-bold m-6 md:text-left">
                            Get to know me!
                        </h1>
                        <p>
                            {" "}
                            Hello everyone! My name is Raymond and I am a{" "}
                            <span className="font-semibold text-blue-600">{"goal-oriented"}</span>,
                            <span className="font-semibold text-blue-600">{" determined"}</span>, and
                            <span className="font-semibold text-blue-600">{" passionate"}</span> software engineer
                            located in Los Angeles, CA.
                        </p>
                        <br />
                        <p>
                            As a software engineer with a focus on full-stack web development
                            I have always been fascinated by the complexity of software since
                            a young age. The intricacies of how it runs and how it is made have
                            piqued my curiosity, which has only grown stronger over time. 
                        </p>
                        <br />
                        <p>
                            I graduated from University of California, Riverside in 2021 with
                            a BA in Psychology. I realized my passion for technology and decided
                            to pursue a career in software engineering. 
                        </p>
                        <br />
                        <p>
                            I love to learn and explore new things, which has led me to pursue various 
                            hobbies and interests. I find joy in reading, working out, traveling, coding 
                            and playing video games.  
                        </p>
                        <br />
                        <p>
                            My goal is to contribute my skills and knowledge to the success of a 
                            company while continously improving my abilities as a software engineer. 
                            I find joy in solving problems and the freedom to approach them in my 
                            own way, which has become a driving force in my life.
                        </p>
                    </div>
                    <div className="md:w-1/2">
                        <h1 className="text-center text-2xl font-bold m-6 md:text-left">My Skills</h1>
                        <div className="flex flex-wrap flex-row justify-center md:justify-start">
                            {skills.map((item, idx) => {
                                return <p key={idx} className="bg-gray-200 px-4 py-2 mr-2 mt-2 text-gray-500 rounded font-semibold">{item.skill}</p>
                            })}
                            {/* <Image 
                            className="hidden md:block md:relative md:bottom-4 md:left-32 md:z-0"
                            src="/download.jpg" 
                            alt="" 
                            width={325}
                            height={325} 
                            /> */}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default AboutSection