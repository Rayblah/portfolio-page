import React from "react"
import Image from "next/image"
import Link from "next/link"
import SlideUp from "./SlideUp"
import { BsArrowUpRightSquare } from "react-icons/bs"
import { AiFillGitlab } from "react-icons/ai"


const projects = [
    {
        name: "Gastronomical Gems",
        description: "Gastronomical Gems is a restaurant web application that allows logged-in users to manage reservations, food items, and order food.",
        image: "/Gastronomical-gems.png",
        gitlab: "https://gitlab.com/backofthehouse/gastronomical-gems",
        link: "https://backofthehouse.gitlab.io/gastronomical-gems/", 
    },
    {
        name: "Car Sales & Servicing",
        description: "Car sales & servicing is a web application that allows users to manage vehicle inventory, service, sales, appointments",
        image: "/carcar.png",
        gitlab: "https://gitlab.com/Rayblah/car-sales-and-servicing",
        link: "https://gitlab.com/Rayblah/car-sales-and-servicing"
    },
    {
        name: "Task Manager",
        description: "This is a simple web application that allows users to create tasks, to-do lists and set reminders",
        image: "/taskmanager.png",
        gitlab: "https://gitlab.com/Rayblah/task-manager",
        link: "https://gitlab.com/Rayblah/task-manager",
    },
]

const ProjectsSection = () => {
    return (
      <section id="projects">
        <h1 className="my-10 text-center font-bold text-4xl">
          Projects
          <hr className="w-6 h-1 mx-auto my-4 bg-blue-500 border-0 rounded"></hr>
        </h1>
  
        <div className="flex flex-col space-y-28">
          {projects.map((project, idx) => {
            return (
              <div key={idx}>
                <SlideUp offset="-300px 0px -300px 0px">
                  <div className="flex flex-col  animate-slideUpCubiBezier animation-delay-2 md:flex-row md:space-x-12">
                    <div className=" md:w-1/2">
                      <Link href={project.link}>
                        <Image
                          src={project.image}
                          alt=""
                          width={1000}
                          height={1000}
                          className="rounded-xl shadow-xl hover:opacity-70"
                        />
                      </Link>
                    </div>
                    <div className="mt-8 md:w-1/2">
                      <h1 className="text-4xl font-bold mb-6">{project.name}</h1>
                      <p className="text-xl leading-7 mb-4 text-neutral-600 dark:text-neutral-400">
                        {project.description}
                      </p>
                      <div className="flex flex-row align-bottom space-x-4">
                        <Link href={project.gitlab} target="_blank">
                          <AiFillGitlab
                            size={30}
                            className="hover:-translate-y-1 transition-transform cursor-pointer"
                          />
                        </Link>
                        <Link href={project.link} target="_blank">
                          <BsArrowUpRightSquare
                            size={30}
                            className="hover:-translate-y-1 transition-transform cursor-pointer"
                          />
                        </Link>
                      </div>
                    </div>
                  </div>
                </SlideUp>
              </div>
            )
          })}
          
        </div>
      </section>
    )
  }

export default ProjectsSection